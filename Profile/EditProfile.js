
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import myStyle from '../style'

class EditProfile extends Component {

    goBack = () => {
        this.props.history.push('/profile')
    }

    render() {
        const { profile } = this.props
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Edit Profile
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>
                    <View>
                        <Text style={myStyle.bodyText}>Username :</Text>
                    </View>
                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput style={myStyle.bodyTextContent} maxLength={20} placeholder='Username'>ASD</TextInput>
                    </View>
                    <View>
                        <Text style={myStyle.bodyText}>Firstname :</Text>
                    </View>
                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput style={myStyle.bodyTextContent} maxLength={20} placeholder='Firstname'>A</TextInput>
                    </View>
                    <View>
                        <Text style={myStyle.bodyText}>Lastname :</Text>
                    </View>
                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput style={myStyle.bodyTextContent} maxLength={20} placeholder='Lastname'>B</TextInput>
                    </View>
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>Save</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

export default EditProfile