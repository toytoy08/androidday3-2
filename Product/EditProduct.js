
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, TextInput,Image } from 'react-native';
import myStyle from '../style'

class EditProduct extends Component {

    goBack = () => {
        this.props.history.push('/product')
    }

    render() {
        const { product } = this.props
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Edit Product
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>

                    <TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ width: 300, height: 200 }} source={{ uri: 'https://www.almau.edu.kz/img/no_image.png' }} />
                        </View>
                    </TouchableOpacity>

                    <View>
                        <Text style={myStyle.bodyText}>Name</Text>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput style={myStyle.bodyTextContent} maxLength={20} placeholder='Product name'>test</TextInput>
                    </View>
                    
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>{'Save'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

export default EditProduct