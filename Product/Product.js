
import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import myStyle from '../style'

class Product extends Component {

    goBack = () => {
        this.props.history.push('/main')
    }

    goEdit = () => {
        this.props.history.push('/product/edit')
    }

    render() {
        const { product } = this.props
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Product
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ width: 300, height: 200 }} source={{ uri: 'https://www.almau.edu.kz/img/no_image.png' }} />
                    </View>
                    <View>
                        <Text style={myStyle.bodyText}>Name</Text>
                        <Text style={myStyle.bodyTextContent}>test</Text>
                    </View>
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity style={{}} onPress={() => { this.goEdit() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>{'Edit'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

export default Product