
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet,TextInput } from 'react-native';
import myStyle from '../style'

class AddProduct extends Component {

    goBack = () => {
        this.props.history.push('/main')
    }

    render() {
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Add Product
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>
                    <View>
                        <Text style={myStyle.bodyText}>Image</Text>
                    </View>
                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput maxLength={20} placeholder='Image'></TextInput>
                    </View>
                    <View>
                        <Text style={myStyle.bodyText}>Product</Text>
                    </View>
                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput maxLength={20} placeholder='Product name'></TextInput>
                    </View>
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>{'Save'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

export default AddProduct