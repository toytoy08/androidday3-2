
import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';

type Props = {};
class Main extends Component<Props> {

    goToProduct = () => {
        this.props.history.push('/product')
    }

    goToProfile = () => {
        this.props.history.push('/profile')
    }

    goToAddProduct = () => {
        this.props.history.push('/product/add')
    }

    render() {
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.header}>

                    <View style={myStyle.headerViewText}>
                        <TouchableOpacity onPress={() => { this.goToProfile() }} >

                            <View style={myStyle.headerText}>
                                <Text style={myStyle.headerText}>My Profile</Text>
                            </View>

                        </TouchableOpacity>
                    </View>
                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>
                    <ScrollView>

                        <View style={myStyle.row}>
                            <View style={myStyle.box1}>
                                <Text style={myStyle.txtcolor} onPress={() => { this.goToProduct() }}>Product</Text>
                            </View>
                            <View style={myStyle.box2}>
                                <Text style={myStyle.txtcolor}>Product</Text>
                            </View>
                        </View>

                    </ScrollView>
                </View>
                {/* body */}

                {/* footer */}
                <View>
                    <View>
                        <TouchableOpacity style={myStyle.footerTouch} onPress={() => { this.goToAddProduct() }} >
                            <View style={myStyle.footeView}>
                                <Text style={myStyle.footerText}>{'Add'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* footer */}

            </View>

        );
    }
}

const myStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
    },
    header: {
        alignItems: 'center',
        flex: 1,
        backgroundColor:'#FFD027',
        borderWidth: 2,
    },
    headerText: {
        fontSize: 40,
        textAlign: 'center',
        color: '#FFD027',
        padding:5
    },
    headerViewText: {
        flex: 1,
        backgroundColor: '#162C9a',
    },
    body: {
        backgroundColor: 'lightblue',
        flex: 7,
        flexDirection: 'column'
    },

    box1: {
        backgroundColor: 'brown',
        flex: 1,
        margin: 14,
        padding: 15

    },
    box2: {
        backgroundColor: 'black',
        flex: 1,
        margin: 14,
        padding: 15
    },
    row: {
        backgroundColor: 'lightblue',
        flex: 1,
        flexDirection: 'row'
    },
    txtcolor: {
        color: 'white',
        fontSize: 40,
    },
    footeView: {
        padding: 2
    },
    footerTouch: {
        alignItems: 'center',
        backgroundColor: '#162C9a',
        borderWidth: 2,
        padding: 5
    },
    footerText: {
        fontSize: 40,
        textAlign: 'center',
        color: '#FFD027'
    },
});

export default Main