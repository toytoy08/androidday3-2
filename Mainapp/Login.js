import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert, Image, TextInput } from 'react-native';

type Props = {};
export default class Profile extends Component<Props> {

  state = {
    username: '',
    password: ''
  }

  onValueChange = (field, value) => {
    this.setState({ [field]: value })
  }

  checkIdPass = () => {
    if (this.state.username == 'asd' && this.state.password == 'asd') {
      Alert.alert('Login success', 'You are in Main Page')
      this.props.history.push('/main')
    }
    else {
      Alert.alert('WRONG!!!!', 'You lie to me')
    }
  }

  clearIdPass = () => {
    clear();
  }

  render() {
    return (
      <View style={styles.container}>
        {/* Header */}

        <View style={{
          flex: 2,
          justifyContent: 'center'
        }}>

          <View>
            <Image
              style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
              source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
            />
          </View>

        </View>

        {/* Header */}

        {/* Body */}

        <View style={{
          alignItems: 'center',
          flex: 1,
          padding: 5
        }}>

          <View>
            <Text>{this.state.username} - {this.state.password}</Text>
          </View>
          <View>
            <TextInput maxLength={20} style={styles.textin} onChangeText={value => { this.onValueChange("username", value) }} placeholder='Username'></TextInput>
            <TextInput maxLength={20} style={styles.textin} onChangeText={value => { this.onValueChange("password", value) }} placeholder='Password'></TextInput>
          </View>

        </View>

        {/* Body */}

        {/* Footer */}

        <View style={{
          padding: 20,
          flexDirection: 'row',
        }}>

          <View style={{
            padding: 10
          }}>
            <Button
              title={'Login'}
              onPress={() => { this.checkIdPass() }}
            />

          </View>
          <View style={{
            padding: 10
          }}>
            <Button
              title={'Clear'}
            // onPress={() => { this.clear() }}
            />
          </View>

        </View>
        {/* Footer
 */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    alignItems: 'stretch',
    textAlign: 'center',
    width: 250
  },
  textin: {
    borderColor: 'gray',
    borderWidth: 1,
    width: 250,
    textAlign: 'center'
  }
});
